import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state:{
		username: localStorage.getItem('username') ? localStorage.getItem('username') : null,
		email: null,
		loggedIn: localStorage.getItem('token') ? true : false,
		token: localStorage.getItem('token') || null,
		tokenexpiration: localStorage.getItem('token_expiration') || null,
		page: 1
	},
	mutations:{
		loggin(state){
			state.loggedIn = !state.loggedIn
		},
		setusername(state, value){
			state.username = value
		},
		setemail(state, value){
			state.email = value
		},
		settoken(state, value){
			state.token = value
		},
		settokenexpiration(state, value){
			state.tokenexpiration = value
		},
		reset(state){
			state.loggedIn = false
			state.username = null
			state.email = null
			state.token = null
			state.tokenexpiration = null
		},
		definePage(state, page){
			state.page = page
		}
	},
	actions:{
    	checkToken(context){
    		let curTimeStamp = Math.round(Date.now() / 1000)
    		if(window.localStorage.getItem('token')){
    			if(context.state.tokenexpiration != null){
    				if(context.state.tokenexpiration < curTimeStamp){
    					context.commit('reset')
    					localStorage.clear()
    				}
    			}
    		}
   		}
  	}
})