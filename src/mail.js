function to(name, email) {
	const url = 'mail/mail_scr';
	let formData = new FormData(),
	options = {
  		method: 'POST',
  		body: formData
  	};
  	formData.append('name', name);
	formData.append('email', email);

	let req = new Request(url, options);
    
	fetch(req)
		.then((response)=>{
			if(response.ok){
				response.statusText
			}else{
				throw new Error("Failed to send email");	
			}
		})
		.catch((err) =>{
			if(err.message){
				console.log(err.message)
			}else{
				console.log(err)
			}
		});
}

export default {
	to
};