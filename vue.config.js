const { resolve } = require('path');
module.exports = {
    devServer: {
    hot: true,
    contentBase: resolve(__dirname, 'dist'),
    publicPath: '/',
    proxy: {
       '/api': {
        target: 'http://localhost/api/api',
        pathRewrite: { '^/api': '' },
      }
    },
  },
  css: {
    loaderOptions: {
      sass: {
        data: `@import "@/assets/variables.scss";`
      }
    }
  }
};